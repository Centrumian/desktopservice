﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DesktopService
{
    static class DBPresentation
    {
        private static string _userPath = "dbUsers.bin";
        private static string _recordPath = "dbRecords.bin";
        private static string _settingsPath = "dbSettings.bin";
        private static BinaryFormatter _bf;

        public static User CurrentUser { set; get; }

        static DBPresentation()
        {
            _bf = new BinaryFormatter();
            if (!File.Exists(_userPath))
            {
                using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
                {
                    HashSet<User> users = new HashSet<User>();
                    users.Add(new User("admin", "admin"));
                    _bf.Serialize(fs, users);
                }
            }
            _bf = new BinaryFormatter();
            if (!File.Exists(_recordPath))
            {
                using (FileStream fs = new FileStream(_recordPath, FileMode.OpenOrCreate))
                {
                    HashSet<RecordTable> records = new HashSet<RecordTable>();
                    _bf.Serialize(fs, records);
                }
            }
            _bf = new BinaryFormatter();
            if (!File.Exists(_settingsPath))
            {
                using (FileStream fs = new FileStream(_settingsPath, FileMode.OpenOrCreate))
                {
                    Dictionary<string, double> _price = new Dictionary<string, double>();
                    _price.Add("Мойка", 300);
                    _price.Add("TO", 1500);
                    _price.Add("Ремонт", 2000);
                    _price.Add("Прочее", 500);

                    EditorSettings settings = new EditorSettings("На данный момент нет акций", _price);
                    _bf.Serialize(fs, settings);
                }
            }
        }

        public static HashSet<User> GetUsers()
        {
            using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
            {
                return (HashSet<User>)_bf.Deserialize(fs);
            }
        }

        public static void AddUser(User user)
        {
            HashSet<User> _current;
            using (FileStream fs = new FileStream(_userPath, FileMode.OpenOrCreate))
            {
                _current =  (HashSet<User>)_bf.Deserialize(fs);
            }
            _current.Add(user);
            using (FileStream fs = new FileStream(_userPath, FileMode.Create))
            {
                _bf.Serialize(fs, _current);
            }

        }

        public static HashSet<RecordTable> GetRecords()
        {
            using (FileStream fs = new FileStream(_recordPath, FileMode.OpenOrCreate))
            {
                return (HashSet<RecordTable>)_bf.Deserialize(fs);
            }
        }

        public static void AddRecord(RecordTable record)
        {
            HashSet<RecordTable> _current;
            using (FileStream fs = new FileStream(_recordPath, FileMode.OpenOrCreate))
            {
                _current = (HashSet<RecordTable>)_bf.Deserialize(fs);
            }
            _current.Add(record);
            using (FileStream fs = new FileStream(_recordPath, FileMode.Create))
            {
                _bf.Serialize(fs, _current);
            }
        }

        public static void DeleteRecord(RecordTable record)
        {
            HashSet<RecordTable> _current;
            using (FileStream fs = new FileStream(_recordPath, FileMode.OpenOrCreate))
            {
                _current = (HashSet<RecordTable>)_bf.Deserialize(fs);
            }
            RecordTable rt = _current.FirstOrDefault(r => r.Date == record.Date);
            _current.Remove(rt);
            using (FileStream fs = new FileStream(_recordPath, FileMode.Create))
            {
                _bf.Serialize(fs, _current);
            }
        }

        public static EditorSettings GetSettings()
        {
            using (FileStream fs = new FileStream(_settingsPath, FileMode.OpenOrCreate))
            {
                return (EditorSettings)_bf.Deserialize(fs);
            }
        }

        public static void SaveSettings(EditorSettings settings)
        {
            using (FileStream fs = new FileStream(_settingsPath, FileMode.Create))
            {
                _bf.Serialize(fs, settings);
            }
        }      
    }
}
