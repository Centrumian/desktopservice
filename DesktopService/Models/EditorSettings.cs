﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopService
{
    [Serializable]
    class EditorSettings
    {
        public string CurrentSale { get; set; }

        public Dictionary<string,double> PriceList { get; set; }

        public EditorSettings(string currentSale, Dictionary<string, double> priceList)
        {
            CurrentSale = currentSale;
            PriceList = priceList;
        }
    }
}
