﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopService
{
    [Serializable]
    public class User
    {
        public string Login {  get; private set; }

        public string Password { private set; get; }

        public User(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}
