﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopService
{
    [Serializable]
    public class RecordTable
    {
        public string Time { get; set; }

        public string DisplayState { get; set; }

        public DateTime Date { get; set; }

        public State State { get; set; }

        public RecordSettings Settings { get; set; }

        public RecordTable(DateTime time)
        {
            Time = time.ToShortTimeString();
            Date = time;
            State = State.Free;
            DisplayState = "Свободно";
        }

        public void BookRecord(string clientName, string comment, string auto, string work, string phone)
        {
            Settings = new RecordSettings(clientName, comment, auto, work, phone);
            State = State.Engaged;
            DisplayState = "Занято";
        }
    }

    [Serializable]
    public enum State
    {
        Free = 0,
        Engaged = 1
    }
}
