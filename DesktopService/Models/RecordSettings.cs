﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopService
{
    [Serializable]
    public class RecordSettings
    {
        public string ClientName { get; set; }

        public string Comment { get; set; }

        public string Auto { get; set; }

        public string Work { get; set; }

        public string Phone { get; set; }

        public RecordSettings(string clientName, string comment, string auto, string work, string phone)
        {
            Work = work;
            ClientName = clientName;
            Comment = comment;
            Auto = auto;
            Phone = phone;
        }
    }
}
