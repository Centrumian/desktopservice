﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for SaleInfoxaml.xaml
    /// </summary>
    public partial class SaleInfo : Window
    {
        public SaleInfo()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _xamlInfoTb.Text = DBPresentation.GetSettings().CurrentSale;
        }
    }
}
