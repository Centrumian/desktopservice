﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for SettingsEditWindow.xaml
    /// </summary>
    public partial class SettingsEditWindow : Window
    {
        public SettingsEditWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            EditorSettings es = DBPresentation.GetSettings();
            List<SettingTableItem> _items = new List<SettingTableItem>();
            foreach(var k in es.PriceList.Keys)
            {
                _items.Add(new SettingTableItem(k, es.PriceList[k]));
            }
            _xamlDataGrid.ItemsSource = _items;
            _xamlInfoTb.Text = es.CurrentSale;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, double> _newPriceList = new Dictionary<string, double>();
            foreach(var i in _xamlDataGrid.Items)
            {
                SettingTableItem _sti = i as SettingTableItem;
                _newPriceList.Add(_sti.Work, _sti.Price);
            }

            EditorSettings newEs = new EditorSettings(_xamlInfoTb.Text, _newPriceList);
            DBPresentation.SaveSettings(newEs);
            DialogResult = true;
        }
    }

    class SettingTableItem
    {
        public string Work { get; set; }

        public double Price { get; set; }

        public SettingTableItem(string work, double price)
        {
            Work = work;
            Price = price;
        }
    }
}
