﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DBPresentation.GetUsers().Any(user => user.Login == NewLoginTb.Text))
            {
                MessageBox.Show("Пользователь с таким логином уже существует!");
            }
            else if (NewPasswordBox.Password != RepPasswordBox.Password)
            {
                MessageBox.Show("Пароли не совпадают!");
            }
            else
            {
                var users = DBPresentation.GetUsers();
                User newUser = new User(NewLoginTb.Text, NewPasswordBox.Password);
                DBPresentation.AddUser(newUser);
                DBPresentation.CurrentUser = newUser;
                DialogResult = true;
            }             
        }
    }
}
