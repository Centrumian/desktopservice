﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {
        public AuthWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (new RegisterWindow().ShowDialog() == true)
            {
                DialogResult = true;
            }
        }

        private void EnterBtn_Click(object sender, RoutedEventArgs e)
        {
            User u = DBPresentation.GetUsers().FirstOrDefault(us => us.Login == loginTb.Text && us.Password == passwordb.Password);
            if (u == null)
            {
                MessageBox.Show("Неверная комбинация логин/пароль");
            }
            else
            {
                DBPresentation.CurrentUser = u;
                DialogResult = true;
            }
        }
    }
}
