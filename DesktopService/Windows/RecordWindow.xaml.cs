﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for RecordWindow.xaml
    /// </summary>
    public partial class RecordWindow : Window
    {
        private RecordTable _recordTable;

        public RecordWindow(RecordTable rt)
        {
            InitializeComponent();
            _recordTable = rt;
            _xamlSelectedDate.Text = _recordTable.Date.ToShortDateString() + " в " + _recordTable.Date.ToShortTimeString();
            _xamlAutoModelCmb.Items.Add("Audi");
            _xamlAutoModelCmb.Items.Add("BMW");
            _xamlAutoModelCmb.Items.Add("Chevrolet");
            _xamlAutoModelCmb.Items.Add("Ford");
            _xamlAutoModelCmb.Items.Add("KIA");
            _xamlAutoModelCmb.Items.Add("Mazda");
            _xamlAutoModelCmb.Items.Add("Nissan");
            _xamlAutoModelCmb.Items.Add("Renault");
            _xamlAutoModelCmb.Items.Add("Toyota");
            _xamlAutoModelCmb.Items.Add("Volkswagen");
            _xamlAutoModelCmb.Items.Add("ВАЗ (LADA)");
            _xamlAutoModelCmb.Items.Add("Другой автомобиль");

            var settings = DBPresentation.GetSettings();
            foreach (var k in DBPresentation.GetSettings().PriceList.Keys)
            {
                var _val = settings.PriceList[k];
                _xamlWorkClassCmb.Items.Add(k + " от " + _val.ToString() + " p.");
            }   
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(_xamlFullNameTb.Text))
            {
                MessageBox.Show("Укажите своё имя!");
            }
            else if (_xamlAutoModelCmb.SelectedItem == null)
            {
                MessageBox.Show("Выберите марку своего автомобиля");
            }
            else if (_xamlWorkClassCmb.SelectedItem == null)
            {
                MessageBox.Show("Выберите класс работ");
            }
            else if (string.IsNullOrEmpty(_xamlPhoneNumber.Text))
            {
                MessageBox.Show("Укажите номер телефона");
            }
            else
            {
                string _work = _xamlWorkClassCmb.SelectedItem.ToString();
                // размещение одной работы на двух строках
                if (_work.Contains("TO") || _work.Contains("Ремонт"))
                {
                    MainWindow _mw = (MainWindow)Owner;
                    var ind = _mw.recordDataGrid.SelectedIndex;
                    if (ind < _mw.recordDataGrid.Items.Count - 1)
                    {
                        _recordTable.BookRecord(_xamlFullNameTb.Text,
                                           string.IsNullOrEmpty(_xamlCommentTb.Text) ? "" : _xamlCommentTb.Text,
                                           _xamlAutoModelCmb.SelectedItem.ToString(),
                                           _work,
                                           _xamlPhoneNumber.Text);
                        DBPresentation.AddRecord(_recordTable);
                        RecordTable _rt2 = (RecordTable)_mw.recordDataGrid.Items[ind + 1];
                        _rt2.BookRecord(_xamlFullNameTb.Text,
                                       string.IsNullOrEmpty(_xamlCommentTb.Text) ? "" : _xamlCommentTb.Text,
                                       _xamlAutoModelCmb.SelectedItem.ToString(),
                                       _work,
                                       _xamlPhoneNumber.Text);
                        DBPresentation.AddRecord(_rt2);
                        DialogResult = true;
                        MessageBox.Show("Спасибо за то, что выбрали наш сервис!\nНаш оператор скоро свяжется с Вами");
                    }
                    else
                    {
                        MessageBox.Show("Данный класс работы рассчитан на два часа. К сожалению, на такое время бронь невозможна");
                    }
                }
                else
                {
                    _recordTable.BookRecord(_xamlFullNameTb.Text,
                                            string.IsNullOrEmpty(_xamlCommentTb.Text) ? "" : _xamlCommentTb.Text,
                                            _xamlAutoModelCmb.SelectedItem.ToString(),
                                            _work,
                                            _xamlPhoneNumber.Text);
                    DBPresentation.AddRecord(_recordTable);
                    DialogResult = true;
                    MessageBox.Show("Спасибо за то, что выбрали наш сервис!\nНаш оператор скоро свяжется с Вами");
                }
            }
        }

        private void _xamlPhoneNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9.-]+");
            if (_regex.IsMatch(e.Text))
                e.Handled = true;
        }
    }
}
