﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for RecordEditor.xaml
    /// </summary>
    public partial class RecordEditorWindow : Window
    {
        private RecordTable _recordTable;

        public RecordEditorWindow(RecordTable recordTable)
        {
            InitializeComponent();
            _recordTable = recordTable;
        }

        private void _xamlEditBtn_Click(object sender, RoutedEventArgs e)
        {
            DBPresentation.DeleteRecord(_recordTable);
            _recordTable.Settings.ClientName = _xamlFullNameTb.Text;
            _recordTable.Settings.Comment = _xamlCommentTb.Text;
            _recordTable.Settings.Auto = _xamlAutoTb.Text;
            _recordTable.Settings.Work = _xamlWorkClassTb.Text;
            DBPresentation.AddRecord(_recordTable);
            MessageBox.Show("Данная запись была изменена!");
        }

        private void _xamlDeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            DBPresentation.DeleteRecord(_recordTable);
            MessageBox.Show("Данная запись была удалена!");
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _xamlSelectedDate.Text = _recordTable.Date.ToShortDateString() + " на " + _recordTable.Date.ToShortTimeString();
            _xamlFullNameTb.Text = _recordTable.Settings.ClientName;
            _xamlCommentTb.Text = _recordTable.Settings.Comment;
            _xamlAutoTb.Text = _recordTable.Settings.Auto;
            _xamlWorkClassTb.Text = _recordTable.Settings.Work;
        }

        private void _xamlPhoneNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9.-]+");
            if (_regex.IsMatch(e.Text))
                e.Handled = true;
        }
    }
}
