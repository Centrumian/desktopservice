﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesktopService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            if (new AuthWindow().ShowDialog() == false)
            {
                this.Close();
            }
            DataContext = DBPresentation.CurrentUser;
            if (DBPresentation.CurrentUser.Login == "admin")
            {
                _xamlEditorBtn.Visibility = Visibility.Visible;
                _xamlSettingEditBtn.Visibility = Visibility.Visible;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {        
            datePicker.SelectedDate = DateTime.Now;
            RefreshTableData();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            object obj = recordDataGrid.SelectedItem;
            if (obj == null)
            {
                MessageBox.Show("Выберите удобное для вас время!");
            }
            else if ((obj as RecordTable).State == State.Engaged)
            {
                MessageBox.Show("К сожалению, выбранное вами время уже забронировано!");
            }
            else
            {
                RecordTable rt = (RecordTable)recordDataGrid.SelectedItem;
                RecordWindow _recWin = new RecordWindow(rt);
                _recWin.Owner = this;
                if (_recWin.ShowDialog() == true)
                {
                    RefreshTableData();
                }
            }
        }

        private void datePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshTableData();
        }

        private void RefreshTableData()
        {
            List<RecordTable> records = new List<RecordTable>();
            HashSet<RecordTable> _filledRecords = DBPresentation.GetRecords();
            for (int i = 0; i < 9; i++)
            {
                DateTime _t = new DateTime(datePicker.SelectedDate.Value.Year, datePicker.SelectedDate.Value.Month, datePicker.SelectedDate.Value.Day, 10 + i, 0, 0);
                var f = _filledRecords.FirstOrDefault(rt => rt.Date == _t);
                if (f == null)
                    records.Add(new RecordTable(_t));
                else if (f is RecordTable rt)
                    records.Add(rt);
            }
            recordDataGrid.ItemsSource = records;
        }

        private void _xamlEditorBtn_Click(object sender, RoutedEventArgs e)
        {
            object obj = recordDataGrid.SelectedItem;
            if (obj == null)
            {
                MessageBox.Show("Выберите время для редактирования!");
            }
            else if (obj is RecordTable rt && rt.State == State.Free)
            {
                MessageBox.Show("На это время записи нет. Нет свойств для редактирования");
            }
            else
            {
                new RecordEditorWindow(obj as RecordTable).ShowDialog();
                RefreshTableData();
            }
        }

        private void _xamlSaleBtn_Click(object sender, RoutedEventArgs e)
        {
            new SaleInfo().ShowDialog();
        }

        private void _xamlSettingEditBtn_Click(object sender, RoutedEventArgs e)
        {
            new SettingsEditWindow().ShowDialog();
        }
    }
}
